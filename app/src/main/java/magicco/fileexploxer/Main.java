package magicco.fileexploxer;

import java.io.File;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class Main extends Activity implements OnClickListener
{
    private static final int REQUEST_PICK_FILE = 1; // Definisco il codice dell'azione da passare all'attività explorer

    private TextView txvPath;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Button btnBrowse;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txvPath = (TextView)findViewById(R.id.textView_path);

        btnBrowse = (Button)findViewById(R.id.button_browse);
        btnBrowse.setOnClickListener(this);

    }

    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.button_browse:
                Intent intent = new Intent(this, Explorer.class);
                startActivityForResult(intent, REQUEST_PICK_FILE); // Avvio Explorer dicendogli che mi aspetto un ritorno
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        File file;

        if(resultCode == RESULT_OK) // Verifico la riuscita del ritorno
        {
            switch(requestCode) // Verifico il ritorno in base ai codici delle richieste
            {
                case REQUEST_PICK_FILE:
                    if(data.hasExtra(Explorer.EXTRA_FILE_PATH))
                    {
                        file = new File(data.getStringExtra(Explorer.EXTRA_FILE_PATH)); // Genero il file dal path ritornatomi
                        txvPath.setText(file.getPath()); // Stampo il path
                    }
                    break;
            }
        }
    }
}
