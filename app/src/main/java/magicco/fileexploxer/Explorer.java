package magicco.fileexploxer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class Explorer extends ListActivity
{
    public final static String EXTRA_FILE_PATH = "file_path"; // Path del file che ritorno
    public final static String EXTRA_SHOW_HIDDEN_FILES = "show_hidden_files";
    public final static String EXTRA_ACCEPTED_FILE_EXTENSIONS = "accepted_file_extensions";
    private final static String DEFAULT_INITIAL_DIRECTORY = "/storage/emulated/0/"; // Directory di partenza

    protected File directory; // Direcory corrente
    protected ArrayList<File> files; // File della directory corrente
    protected ExplorerAdapter explorerAdapter; // Adapter della listView
    protected boolean showHiddenFiles = false; // Attivo file nascosti
    protected String[] acceptedFileExtensions; // Estensioni dei file accettati

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Definisco view_empty come layput e lo imposto in caso di lista vuota
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View emptyView = inflator.inflate(R.layout.view_empty, null);
        ((ViewGroup) getListView().getParent()).addView(emptyView);
        getListView().setEmptyView(emptyView);

        directory = new File(DEFAULT_INITIAL_DIRECTORY); // Imposto la directory iniziale

        files = new ArrayList<File>(); // Inizializzo l'ArrayList

        // Imposto il listAdapter
        explorerAdapter = new ExplorerAdapter(this, files);
        setListAdapter(explorerAdapter);

        acceptedFileExtensions = new String[] {};   // Inizializzo l'array di estensioni, vuoto permette qualunque estensione

        // Leggo i dati passatti all'activity
        if(getIntent().hasExtra(EXTRA_FILE_PATH))
            directory = new File(getIntent().getStringExtra(EXTRA_FILE_PATH));
        if(getIntent().hasExtra(EXTRA_SHOW_HIDDEN_FILES))
            showHiddenFiles = getIntent().getBooleanExtra(EXTRA_SHOW_HIDDEN_FILES, false);
        if(getIntent().hasExtra(EXTRA_ACCEPTED_FILE_EXTENSIONS))
        {
            ArrayList<String> collection = getIntent().getStringArrayListExtra(EXTRA_ACCEPTED_FILE_EXTENSIONS);
            acceptedFileExtensions = collection.toArray(new String[collection.size()]);
        }
    }

    @Override
    protected void onResume() // Gestione della ripresa dell'esecuzione dell'applicazione
    {
        refreshFilesList(); // Aggiorno la listView aggiornando i file nella ArrayList
        super.onResume();
    }

    protected void refreshFilesList() // Aggiorno il contenuto dell'ArrayList leggendo i file della directory corrente
    {
        files.clear();

        ExtensionFilenameFilter filter = new ExtensionFilenameFilter(acceptedFileExtensions); // Genero un filtro per i file basato sull'estensioni

        File[] files = directory.listFiles(filter); // Inizializzo i file in bas al filtro

        // Aggiungo i file all'ArrayList
        if(files != null && files.length > 0)
        {
            for(File f : files)
            {
                if(f.isHidden() && !showHiddenFiles) // Se il file è nascosto e io non sono interessato
                {
                    continue;
                }

                this.files.add(f);
            }

            Collections.sort(this.files, new FileComparator()); // Ordino i file attraverso una classe da me definita
        }

        explorerAdapter.notifyDataSetChanged(); // Segnalo modifiche alla listView così che si aggiorna
    }

    @Override
    public void onBackPressed() // Gestione del caso di "Back", torno alla directory precedente
    {
        if(directory.getParentFile() != null) // Controllo di non essere alla directory di base
        {
            directory = directory.getParentFile(); // Imposto la directory corrente con la directory padre
            refreshFilesList(); // Aggiorno la listView
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) // Listener della listView
    {
        File newFile = (File)l.getItemAtPosition(position); // Genero il file cliccato

        // Verifico se è un file o una directory
        if(newFile.isFile()) // E' un file, quindi ho terminato
        {
            // Genero l'intent da restituire con il path e il risultato dell'operazione
            Intent extra = new Intent();
            extra.putExtra(EXTRA_FILE_PATH, newFile.getAbsolutePath());
            setResult(RESULT_OK, extra);
            finish();
        }
        else
        {
            directory = newFile; // Imposto la directory corrente
            refreshFilesList(); // Aggiorno
        }

        super.onListItemClick(l, v, position, id);
    }

    // Adapter che gestisce la generazione della listView
    private class ExplorerAdapter extends ArrayAdapter<File>
    {
        private List<File> items; // Lista dei file

        public ExplorerAdapter(Context context, List<File> objects)
        {
            super(context, R.layout.row_file, android.R.id.text1, objects);
            items = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View row;

            // Definisco il layout
            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.row_file, parent, false);
            }
            else
                row = convertView;

            File object = items.get(position); // Definisco il file in base alla posizione

            ImageView imageView = (ImageView)row.findViewById(R.id.imageView_file);

            TextView textView = (TextView)row.findViewById(R.id.textView_name);
            textView.setSingleLine(true);
            textView.setText(object.getName());

            // Imposto l'incona in base a se è un file o una directory
            if(object.isFile())
                imageView.setImageResource(R.drawable.ic_file);
            else
                imageView.setImageResource(R.drawable.ic_folder);

            return row;
        }
    }

    // Classe per l'ordinamento dei file
    private class FileComparator implements Comparator<File>
    {
        public int compare(File f1, File f2)
        {
            if(f1 == f2)
                return 0;

            if(f1.isDirectory() && f2.isFile()) // Mostra le directory sopra i file
                return -1;

            if(f1.isFile() && f2.isDirectory()) // Mostro file sotto le directory
                return 1;

            return f1.getName().compareToIgnoreCase(f2.getName()); // Ordino le directory in ordine alfabetico
        }
    }

    // Classe per il filtro fei file in base alle estensioni
    private class ExtensionFilenameFilter implements FilenameFilter
    {
        private String[] extensions;

        public ExtensionFilenameFilter(String[] extensions)
        {
            super();
            this.extensions = extensions;
        }

        public boolean accept(File dir, String filename)
        {
            if(new File(dir, filename).isDirectory())
            {
                // Accetto tutte le directory
                return true;
            }

            // Verifico i file in base all'estensione
            if(extensions != null && extensions.length > 0)
            {
                for(int i = 0; i < extensions.length; i++) // Verifico per tutte le estensioni
                {
                    if(filename.endsWith(extensions[i]))
                    {
                        // Il file un estensione accettata
                        return true;
                    }
                }

                return false; // Il file non matcha nessuna estensione
            }

            return true; // Se nessuna estensione è impostata, accetto tutti i file
        }
    }
}
